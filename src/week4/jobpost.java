package week4;

public class jobpost {

// Assign values to be used
    private String applicant = "Evan Skinner";
    private String title = "Network Administrator";
    private String description = "Administrate our network for lots of money.";
    private int salary = 1000001;
    

    public String toString(){
        StringBuilder out = new StringBuilder();

        out.append("\napplicant: ").append(applicant);
        out.append("\ntitle: ").append(title);
        out.append("\ndescription: ").append(description);
        out.append("\nsalary: $").append(salary);

        return out.toString();
    }
    
    public String getName() {
        return applicant;
    }
    public void setName(String applicant) {
        this.applicant = applicant;
    }
    public String getDesignation() {
        return title;
    }
    public void setDesignation(String title) {
        this.title = title;
    }
    public String getDepartment() {
        return description;
    }
    public void setDepartment(String description) {
        this.description = description;
    }
    public int getSalary() {
        return salary;
    }
    public void setSalary(int salary) {
        this.salary = salary;
    }
}