package week4;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class server {

    public static void main(String args[]) throws IOException {

        ServerSocket server = new ServerSocket(8080);
        System.out.println("Listening for connection on port 8080 ....");
        jobpost jp = new jobpost();
        ObjectMapper mapperObj = new ObjectMapper();
        while (true) {
            try (Socket socket = server.accept()) {
                String json = mapperObj.writeValueAsString(jp);
                System.out.println(json);
                String httpResponse = "HTTP/1.1 200 OK\r\n\r\n" + json;
                socket.getOutputStream().write(httpResponse.getBytes("UTF-8"));
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}