//Package name.
package week2;

//Imports all tools needed from the util package.
import java.util.*;

//Declare the class named tree.
public class collection {
    // Line is needed to start a java program.
    public static void main(String[] args) {

        System.out.println("-----THE LIST-----" + "\n");
        // First method of the class
        // Creates a Linked List
        List<String> fruit = new LinkedList<String>();
        // Adds contents to linked list
        fruit.add("Pineapple");
        fruit.add("Apple");
        fruit.add("Pear");
        fruit.add("Peach");
        fruit.add("Strawberry");
        fruit.add("Mango");
        fruit.add("Raspberry");
        fruit.add("Banana");

        // Prints List
        System.out.println(fruit + "\n");

        // Sorts the list in alphabetical order
        fruit.sort(Comparator.comparing(String::toString));
        for (Object str : fruit) {
            System.out.println((String) str);
        }

        System.out.println("\n");
        // Deletes what was stored in location 3
        fruit.remove(3);
        System.out.println(fruit + "\n");

        fruit.clear();
        System.out.println(fruit);

        // End of lists

        System.out.println("-----THE SET-----" + "\n");

        Set set = new HashSet();
        set.add("Beans");
        set.add("Chicken");
        set.add("Bread");
        set.add("Cake");
        set.add("Milk");
        // Sets can only contain unique elements.
        // The following element will not be added.
        set.add("Milk");

        System.out.println(set);

        // This section removes the comma from list.
        for (Object str : set) {
            System.out.println((String) str);
        }

        System.out.println("-----THE QUEUE----" + "\n");

        Queue queue = new LinkedList();
        queue.add("Blue");
        queue.add("Purple");
        queue.add("Yellow");
        queue.add("Green");
        queue.add("Black");
        queue.add("Chartreuse");

        String rem = (String) queue.remove();
        String head = (String) queue.peek();
        int size = queue.size();

        System.out.println("Everything in the Q: " + queue);
        System.out.println(rem + " will be deleted from the queue");
        System.out.println(head + " is now at the beginning of the queue");
        System.out.println("The queue after element has been deleted " + queue);
        System.out.println("The queue now has " + size + " elements" + "\n");

        System.out.println("-----THE TREE-----" + "\n");

        TreeSet<Integer> tree = new TreeSet<Integer>();
        tree.add(01110);
        tree.add(-900019);
        tree.add(1231);
        tree.add(5);
        tree.add(110);
        tree.add(44);
        tree.add(333);
        System.out.println("Elements in TreeSet: " + tree);
        System.out.println("Total size of the tree: " + tree.size());
        System.out.println("The heighest number: " + tree.pollLast());
        System.out.println("The lowest number: " + tree.pollFirst());
        System.out.println("New TreeSet: " + tree);
        System.out.println("Number in First element: " + tree.first());
        System.out.println("Last element: " + tree.last());
        System.out.println("New size: " + tree.size() + "\n");

        System.out.println("-----List using Generics-----" + "\n");
        List<Req> active_request = new LinkedList<Req>();
        active_request.add(new Req(201, "Connect fiber to patch panel"));
        active_request.add(new Req(23, "Reimage laptop with Win10"));
        active_request.add(new Req(12, "Disable Matt's account in Active Directory"));
        active_request.add(new Req(59, "Install new phone in room 14"));
        active_request.add(new Req(3, "Inventory equipment in storage"));

        for (Req ticket : active_request) {
            System.out.println(ticket);
        }
    }

}
