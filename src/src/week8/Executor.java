package week8;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Executor {

	public static void main(String[] args) {
		addThreads();
	}
	public static void addThreads() {
		ExecutorService events = Executors.newFixedThreadPool(5);

				events.execute(new RunningLate(" I'm going to be late!"));
				events.execute(new LeaveForWork());
				events.execute(new Time());
				events.execute(new CheckTime());

		events.shutdown();	
	}
}