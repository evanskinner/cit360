package week8;
import java.text.DateFormat;
import java.util.*;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.SECONDS;

public class CheckTime implements Runnable{
	
	public void run() {
		Date rightNow;
		Locale currentLocale;
		DateFormat timeFormatter;
		String outputTime;
		
		rightNow = new Date();
		currentLocale = new Locale("en");
		
		timeFormatter = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocale);
		outputTime = timeFormatter.format(rightNow);
		

		for(int i=1; i<=6;i++) {
			System.out.println("It is " + outputTime + "!");
			try {
				TimeUnit.MILLISECONDS.sleep(3000);
			}catch (InterruptedException e) {
				System.out.println(e);
			}
		}
	}

}


