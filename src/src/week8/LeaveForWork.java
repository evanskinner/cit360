package week8;

import java.util.concurrent.atomic.AtomicInteger;

public class LeaveForWork implements Runnable{

	private AtomicInteger count= new AtomicInteger();
	public void run() {
		for(int i=5;i>0;i--) {
			LeaveForWork(i);
			count.incrementAndGet();
			System.out.println("I need to leave for work in " + i + " minutes");
		}
	}
	public int getCount() {
		return this.count.get();
	}
	private void LeaveForWork(int i) {
		try {
		Thread.sleep(i*300);
		}catch(InterruptedException e) {
		System.out.println(e);
		}
	}
}
