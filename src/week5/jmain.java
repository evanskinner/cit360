package week5;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class jmain {

	@Test
	public void city() {
		String city1 = "Rexburg";
		String city2 = "Rexburg";
		assertSame(city1, city2);
	}


	@Test
	   public void password() {
		  String password1 = new String ("Ih8pa$$w0rds");
	      String password2 = new String ("Ih8pa$$w0rds");
	      assertEquals(password1, password2);
	   }

	@Test
	public void whoistaller() {
		double height1 = 4.11;
		double height2 = 5.5;
		assertFalse(height1 > height2);
		assertTrue (height2 > height1);
	}
	   @Test
	   public void birthplace() {
		  String state1 = null;
		  String state2 = "Idaho";
	      assertNotNull(state2);
	      assertNull(state1);
	   }
	   @Test
	   public void DMVpriority() {
	      String[] priorityA = {"Evan", "Matt", "Allison", "Stacie"};
	      String[] priorityB =  {"Evan", "Matt", "Allison", "Stacie"};
	      assertArrayEquals(priorityA, priorityB);
	   }

	@Test
	public void weight() {
		int weight1 = 33;
		int weight2 = 15;
		assertNotSame(weight1, weight2);
	}

	@Test
	public void bloodtype() {
		String type = "O NEG";
		assertThat(type, is("O NEG"));
	}
}
