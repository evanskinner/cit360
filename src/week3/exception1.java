package week3;

import java.util.*;

public class exception1 {

    // Create the main method
    public static void main(String[] args) {
// Gather input from user
        Scanner input = new Scanner(System.in);
// I used double instead of int because it will accept decimal values.
        // Stores values input from user
        double number1 = 0;
        double number2 = 0;
        // Stores the result of the calculation
        double answer = 0;
// Prints a prompt for the user
        System.out.println("Please enter two numbers");
        // This moves the scanner to the next line
        number1 = input.nextDouble();
// Use boolean because we are looking for true/false
        boolean noError = false;
        // Creates a while loop
        while (!noError) try {
            while (number2 == 0) {
                System.out.print("Second number: ");
                number2 = input.nextDouble();
                if (number2 == 0) {
                    System.out.println("This program cannot divide by 0 \n" + "Please re-enter your second number now");
                }
            }
            noError = true;
            answer = divide(number1, number2);
            // Thrown when an exceptional arithmetic condition has occurred.
            // For example, an integer "divide by zero" throws an instance of this class.
        } catch (ArithmeticException e) {
            System.out.println("Re-enter number " + e);
        }
        // Display the result of the calculation
        System.out.println(number1 + " / by " + number2 + " = " + answer);
    }

    static double divide(double nOne, double nTwo) {
        double divide = 0;
        if (nTwo == 0) {
            throw new ArithmeticException();
        } else {
            divide = nOne / nTwo;
            return divide;
        }
    }
}
